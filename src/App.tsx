import { FC, useState } from 'react';
import styled from 'styled-components';
import BackgroundDark from './images/bg-desktop-dark.jpg';
import BackgroundLight from './images/bg-desktop-light.jpg';
import BackgroundDarkMobile from './images/bg-mobile-dark.jpg';
import BackgroundLightMobile from './images/bg-mobile-light.jpg';
import LogoSun from './images/icon-sun.svg';
import LogoMoon from './images/icon-moon.svg';

const Container = styled.div<{ theme: String; }>`
  width: 100vw;
  height: 100vh;
  background-color: ${({ theme }) => theme === 'dark' ? 'hsl(235, 21%, 11%)' : 'hsl(0, 0%, 98%)'};
`;

const Wrapper = styled.div<{ theme: String; }>`
  width: 100%;
  height: 100%;
  background-image: url(${({ theme }) => theme === 'dark' ? BackgroundDark : BackgroundLight});
  background-repeat: no-repeat;
  @media (max-width: 375px) {
    background-image: url(${({ theme }) => theme === 'dark' ? BackgroundDarkMobile : BackgroundLightMobile});
  }
`;

const Content = styled.div`
  position: relative;
  width: 40%;
  height: 100%;
  background-color: transparent;
  margin: 0 auto;
  @media (max-width: 375px) {
    width: 85%;
  }
`;


const Topbar = styled.div`
  width: 100%;
  position: absolute;
  top: 10%;
  display: flex;
  justify-content: space-between;
  transition: all 0.5s;
  @media (max-width: 375px) {
    top: 5%;
  }
`;

const Logo = styled.h1`
  color: hsl(0, 0%, 98%);
  letter-spacing: 1rem;
  font-size: 2.7rem;
  @media (max-width: 375px) {
    font-size: 2rem;
  letter-spacing: 0.7rem;
  }
`;

const ThemeSwitcher = styled.img`
  width: 30px;
  height: 30px;
`;

const App: FC = () => {
  const [theme, setTheme] = useState<String>('dark');

  const toggleThemeSwitcher = () => {
    setTheme(theme === 'dark' ? 'white' : 'dark');
  };

  return (
    <Container theme={theme}>
      <Wrapper theme={theme}>
        <Content>
          <Topbar>
            <Logo>TODO</Logo>
            <ThemeSwitcher src={theme === 'dark' ? LogoSun : LogoMoon} onClick={toggleThemeSwitcher}></ThemeSwitcher>
          </Topbar>
        </Content>
      </Wrapper>
    </Container>
  );
};

export default App;
